import Ember from 'ember';
import layout from '../templates/components/flex-table';

const {run, $, computed, inject} = Ember;
const {htmlSafe} = Ember.String;

export default Ember.Component.extend({
  layout,

  localStorage: inject.service('flex-table-local-storage'),

  classNames: ['flex-table'],

  $thead: null,
  $el: null,
  scrollTop: null,

  columns: [],

  sortAsc: true,
  sortProperty: 'name', 

  tableId: 'flex-table',

  mappedColumns: computed(function(){
    // overwrite widths with those saved in localStorage
    const ls = this.get('localStorage');
    return Ember.A(this.get('columns').map(column => {
      let savedWidth = ls.getColumnWidth(this.get('tableId'), column.id);
      if(savedWidth){
        column.width = savedWidth;
      }

      return Ember.Object.create(column);
    }));
  }),

  rowsSorting: computed('sortAsc', 'sortProperty', function(){
    let sortDir = this.get('sortAsc') ? 'asc' : 'desc';
    return [`${this.get('sortProperty')}:${sortDir}`];
  }),
  sortedRows: computed.sort('filteredRows','rowsSorting'),

  rows: computed('rowData.[]', function(){
    return Ember.A(this.get('rowData').map(row => Ember.Object.create(row)));
  }),

  filteredRows: computed('rows.[]', 'mappedColumns.@each.filterBy', function(){

    let filterColumns = this.get('mappedColumns').filter(column => column.get('filterBy') && column.get('filterBy').length > 0);

    return this.get('rows').filter(row => {
      return filterColumns.every(column => {
        return String(row.get(column.get('key'))).indexOf(column.get('filterBy')) > -1;
      });
    });
  }),

  theadPositioningStyle: computed('scrollLeft', function(){
    return htmlSafe(`left: -${this.get('scrollLeft')}px`);
  }),

  setScrollLeft($el){
    this.set('scrollLeft', $el.scrollLeft());
  },

  moveTableHeaderOnHorizontalScroll($el){
    $el.scroll(() => {
      run.debounce(this, () => {
        this.setScrollLeft($el); 
      }, 0);
    });
  },

  didInsertElement() {
    const $el = $(this.element).find('.flex-table-wrap');
    this.set('$el', $el);
    this.moveTableHeaderOnHorizontalScroll($el);
    this.get('localStorage').initTableData(this.get('tableId'));
  },

  willDestroyElement() {
    this.get('$el').unbind('scroll');
  },

  actions: {
    switchSortProperty(property){
      if(property === this.get('sortProperty')){
        this.toggleProperty('sortAsc');
      } else {
        this.set('sortProperty', property);
      }
    },

    saveColumnWidth(column){
      const ls = this.get('localStorage');
      ls.setColumnWidth(this.get('tableId'), column.id, column.width);
      ls.save();
    }
  }

});
