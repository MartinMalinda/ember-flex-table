import Ember from 'ember';
import layout from '../templates/components/flex-table-row';

export default Ember.Component.extend({
  layout,

  tagName: 'tr'
});
