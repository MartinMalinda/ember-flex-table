import Ember from 'ember';

const {htmlSafe} = Ember.String;

export function colWidthStyle([width]) {
  return htmlSafe(`width: ${width}px`);
}

export default Ember.Helper.helper(colWidthStyle);
