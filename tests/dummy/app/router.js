import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('bootstrap');
  this.route('inline');
  this.route('semantic-ui');
});

export default Router;
