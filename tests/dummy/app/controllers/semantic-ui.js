import Ember from 'ember';

const {inject} = Ember;

export default Ember.Controller.extend({
  application: inject.controller(),
});
